#!Library/Frameworks/Python.framework/Versions/3.9/bin/python3

# root url 

# week broadcast url https://www.kcm.org/watch/tv-broadcast/

# find weekly videos container with tag 'this-week-series-wrapper'
# print video container nodes

# video download url https://traffic.libsyn.com/secure/copelandnetworkvideopodcast/210407_Wed.mp4

import tkinter as tk
from tkinter import ttk
from tkcalendar import *
import time
import sys
import os
from datetime import datetime, timedelta
from video_name_resolver import VideoNameResolver

import requests
from bs4 import BeautifulSoup

from pathlib import Path

class Error(Exception):
    pass
class URLNotFoundError(Error):
    def __init__(self, code=404, reason=""):
        self.status_code=code
        self.reason=reason



ROOT='https://traffic.libsyn.com/secure/copelandnetworkvideopodcast/'
VIDEOS_FOLDER_PATH=f'{Path.home()}/Movies/KCM_Videos/'
VIDEO_EXT = ".mp4"
if Path(VIDEOS_FOLDER_PATH).exists:
    print('Movies/KCM_Videos folder found')
else:
    print("TODO")

print(VIDEOS_FOLDER_PATH)   

class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title('KCM Video Downloader')
        self.resolver = VideoNameResolver()
        self.root_panel = ttk.Frame(self)
        self.root_panel.grid()
        self.root_url_var = tk.StringVar()
        self.root_panel.columnconfigure(0, weight=1)
        self.root_panel.columnconfigure(1, weight=1)
        self.root_panel.columnconfigure(2, weight=1)
        # print(requests.get("https://traffic.libsyn.com/secure/copelandnetworkvideopodcast/210707_Wed.mp4", stream=True))
        self.create_ui()

    def create_ui(self):
        padding = {'padx': 10, 'pady': 10}
        today = datetime.today()
        self.calendar = Calendar(self.root_panel,
            font='Arial 40',
            selectmode='day',
            foreground='darkblue',
            background='gray',
            selectforeground='red',
            showweeknumbers=False,
            showothermonthdays=False)
        print(self.calendar.keys())
        self.calendar.grid(column=1, row=0, **padding)

        self.get_week_btn = ttk.Button(self.root_panel, text='Get Week Videos', command=self.downloadWeekVideos)
        self.get_week_btn.grid(column=1, row=1, **padding)
        self.get_day_btn = ttk.Button(self.root_panel, text='Get Day Video', command=lambda: self.downloadDayVideo(self.calendar.selection_get()))
        self.get_day_btn.grid(column=2, row=1, **padding)
        self.custom_uri = ttk.Button(self.root_panel, text='Get Custom Video Name', command=self.downloadCustomVideoName)
        self.custom_uri.grid(column=0, row=1, **padding)

    def downloadWeekVideos(self):
        d = self.calendar.selection_get()
        for dt, names in self.resolver.names_for_weekof(d):
            self.downloadDayVideo(dt)

    def downloadDayVideo(self,d):
        print(f"============={d}=============")
        for name in self.resolver.names_for_date(d):
            print(f"Trying to download video {name+VIDEO_EXT} for :", d)
            try:
                self.dlvid(name+VIDEO_EXT)
                break
            except URLNotFoundError as e:
                # print("\tVideo not found")
                print("\tStatus Code: ", e.status_code)
                print("\tReason: ", e.reason)
                continue
                

    def dlvid(self,name):
        video_name = name
        url = ROOT+video_name
        r = requests.get(url, stream=True)
        if r.status_code != 200:
            raise URLNotFoundError(code=r.status_code,reason=r.reason)
        elif os.path.isfile(VIDEOS_FOLDER_PATH+video_name):
            print(f'file {video_name} already exists')
        else:
            with open((VIDEOS_FOLDER_PATH+video_name), 'wb') as f:
                print('Dumping "{0}"...'.format(name))
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:
#                        print("writing chunk of ", video_name)
                        f.write(chunk)
                        f.flush()
                print("\tDownloaded Video: ", name)


    def downloadCustomVideoName(self):
        filename = ""
        top=tk.Toplevel(self.root_panel)
        top.wm_title("Custom Video Name")
        entry = tk.Entry(top)
        entry.grid(row=0, columnspan=3, sticky="nsew")
        def get_val():
            filename = entry.get()
            print("trying to download", ROOT+filename)
            try:
                self.dlvid(filename)
                top.destroy()
            except URLNotFoundError as e:
                print("\tStatus Code: ", e.status_code)
                print("\tReason: ", e.reason)
        
        okbtn = tk.Button(top, text="Ok", command=get_val)
        okbtn.grid(column=2,row=1)
        cancel = tk.Button(top, text="Cancel", command=top.destroy)
        cancel.grid(column=0,row=1)
        # top.grid()
        self.root_panel.wait_window(top)

if __name__ == "__main__":
    App().mainloop()
