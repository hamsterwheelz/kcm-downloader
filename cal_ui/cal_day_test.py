import tkinter as tk
from tkinter import ttk
from cal_day import CalDay

root = tk.Tk()
# root.configure(background="black")
# f1 = ttk.Frame(root, width=500, height=500, style="TFrame")
# f1.grid(column=0,row=0)
# c1 = tk.Canvas(root, width=200, height=200, bg="black")
# c1.grid(column=0,row=0)
# c1.grid_propagate(0)
# c1.bind("<Enter>", lambda _: c1.configure(background="green"))
# c1.bind("<Leave>", lambda _: c1.configure(background="black"))
calday = CalDay(root)
calday.onEnter(lambda e: e.widget.configure(bg="green"))
calday.onLeave(lambda e: e.widget.configure(bg="black"))
# calday.addText("Sup")
# c1.create_text(100,100,text="Hello, Hello",fill="green")


# for r in range(4):
# 	c1 = tk.Canvas(root, width=200, height=200, bg="black")
# 	c1.grid(column=r,row=0)
# 	c1.grid_propagate(0)
# 	l1 = lambda e: e.widget.configure(background="green")
# 	l2 = lambda e: e.widget.configure(background="black")
# 	c1.create_text(100,100,text=f'{r}',fill="green")
# 	c1.bind("<Enter>", l1)
# 	c1.bind("<Leave>", l2)

root.mainloop()

# import tkinter
# from tkinter import ttk

# root = tkinter.Tk()
# root.configure(background='black')
# # style configuration
# style = ttk.Style(root)
# style.configure('TLabel', background='black', foreground='white')
# style.configure('TFrame', background='black')

# frame = ttk.Frame(root)
# frame.grid(column=0, row=0)

# ttk.Button(frame, text="Open file", command=None).grid(column=0, row=1)
# lab = ttk.Label(frame, text="test test test test test test ")
# lab.grid(column=0, row=2)


# root.mainloop()