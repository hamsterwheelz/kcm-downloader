import tkinter as tk
from tkinter import ttk




class CalDay:
	def __init__(self, master):
		self.style = ttk.Style(master)
		self.root_frame = ttk.Frame(master, width=100, height=200,)
		self.root_frame.grid(column=0,row=0,sticky="nsew")
		self.cvs = tk.Canvas(self.root_frame, width=self.root_frame['width'],bg="black")
		self.cvs.grid(sticky="nsew")
		# self.root_frame.grid_propagate(0)
	def addText(self, txt):
		l = ttk.Label(self.root_frame, text=txt)
		l.grid(column=0,row=0)

	def onEnter(self, lam):
		self.cvs.bind("<Enter>", lam)

	def onLeave(self, lam):
		self.cvs.bind("<Leave>", lam)