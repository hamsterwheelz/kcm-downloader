import os
import json
from datetime import date, timedelta
from pathlib import Path
from video_name_resolver.constants import EXTENSION_CONFIG_FILENAME

_DEFAULT_CONFIG_DICT = {
	"general-prefix" : [],
	"general-postfix" : [],
	"weekdays" : {
		"monday" : [],
		"tuesday" : [],
		"wednesday" : [],
		"thursday" : [],
		"friday" : []
	}
}

_DATE_DAY_STRS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday']

_DEFAULT_CONFIG_JSON = json.dumps(_DEFAULT_CONFIG_DICT, indent=2)

_PREV_DAY = timedelta(-1)
_NEXT_DAY = timedelta(1)


class VideoNameResolver:
	def __init__(self, _config_file=Path(EXTENSION_CONFIG_FILENAME)):
		self._config_file = _config_file

		if not self._config_file.exists():
			self._extentions = _DEFAULT_CONFIG_DICT.copy()
			self._save_extentions()
		else:
			with self._config_file.open() as f:
				self._extentions = json.loads(f.read())

	def _save_extentions(self):
		with self._config_file.open('w') as f:
			f.write(json.dumps(self._extentions, indent=2))
			f.close()

	def general_postfix_exts(self):
		for ext in self._extentions["general-postfix"]:
			yield ext

	def general_prefix_exts(self):
		for ext in self._extentions["general-prefix"]:
			yield ext
	
	def edit_general_exts(self, lam):
		l = self._extentions["general"]
		lam(l)
		self._save_extentions()

	def day_exts(self, daykey):
		for ext in self._extentions["weekdays"][daykey]:
			yield ext

	def weekday_exts(self):
		for day in self._extentions["weekdays"]:
			yield(day, self.day_exts(day))

	def edit_weekday_exts(self, day, lamb):
		l = self._extentions["weekdays"][day]
		lamb(l)
		self._save_extentions()

	def names_for_date(self, d):
		weekday = d.weekday()
		if weekday > 5:
			return
		weekday = _DATE_DAY_STRS[weekday]
		datestr = d.strftime("%y%m%d")
		yield datestr	

		for gen_ext in self.general_prefix_exts():
			yield gen_ext+"_"+datestr

		for gen_ext in self.general_postfix_exts():
			yield datestr+"_"+gen_ext

		for day_ext in self.day_exts(weekday):
			yield datestr+"_"+day_ext
			for gen_ext in self.general_postfix_exts():
				yield datestr+"_"+day_ext+"_"+gen_ext

	def names_for_weekof(self, d):
		dOfweek = d.weekday()
		mon = d
		if dOfweek != 0:
			mon = d+timedelta(-dOfweek)

		weekdates = [mon + timedelta(x) for x in range(5)]
		for d in weekdates:
			yield d, self.names_for_date(d)
			





		



