import os
import tempfile
import pytest
import json 
from pathlib import Path

from datetime import date, timedelta

from video_name_resolver.constants import EXTENSION_CONFIG_FILENAME
from video_name_resolver.VNR import VideoNameResolver

@pytest.fixture
def config_file_path(tmpdir):
	return Path(tmpdir.join(EXTENSION_CONFIG_FILENAME))



@pytest.fixture
def default_config_dict():
	default = {
		"general": [],
		"weekdays" : {
			"monday": [],
			"tuesday": [],
			"wednesday": [],
			"thursday": [],
			"friday": []
		}
	}
	return default

@pytest.fixture(scope="function")
def config_file(config_file_path, default_config_dict):
	if config_file_path.exists():
		os.remove(config_file_path)

	# print("Creating config file...")
	with config_file_path.open('w') as f:
		assert f.write(json.dumps(default_config_dict, indent=2))
		# print("Created confil file")
		f.close()

	yield config_file_path
	# print("Deleting config file")
	if config_file_path.exists():
		os.remove(config_file_path)
		# if not config_file_path.exists():
		# 	print("Deleted config file")

def test_creates_default_json_file(default_config_dict, config_file_path):
	"""	
	GIVEN VideoNameResolver Instantiated with config_file
	WHEN config_file is not found
	THEN a default config file should be created
	"""
	assert config_file_path.exists()==False

	resolver = VideoNameResolver(_config_file=config_file_path)

	assert config_file_path.exists()==True

	with config_file_path.open() as f:
		assert f.read()==json.dumps(default_config_dict, indent=2)

def test_loads_from_json_file(default_config_dict, config_file_path):
	"""
	GIVEN VideoNameResolver Instantiated with config_file
	WHEN config_file is found
	THENx
	"""

	assert config_file_path.exists()==False

	default_config_dict["general"] = ["loaded"]
	for day in default_config_dict["weekdays"]:
		default_config_dict["weekdays"][day] = ["loaded"]

	with config_file_path.open('w') as f:
		assert f.write(json.dumps(default_config_dict, indent=2))
		f.close()

	assert config_file_path.exists()==True
	
	resolver = VideoNameResolver(_config_file=config_file_path)

	assert "loaded" in resolver.general_postfix_exts()

	for day, day_exts in resolver.weekday_exts():
		assert day in default_config_dict["weekdays"]
		assert "loaded" in day_exts


def test_edit_general_ext(config_file):
	"""
	GIVEN VideoNameResolver instance
	WHEN edit_general_ext method is called with a string
	THEN the string should exist in the general ext list
	"""
	resolver = VideoNameResolver(_config_file=config_file)
	tst_str = "testStr"
	resolver.edit_general_exts(lambda l: l.append(tst_str))
	assert tst_str in resolver.general_postfix_exts()

	new_resolver = VideoNameResolver(_config_file=config_file)
	assert tst_str in new_resolver.general_postfix_exts()

def test_edit_weekday_ext(default_config_dict, config_file):
	"""
	GIVEN 
	WHEN
	THEN
	"""
	resolver = VideoNameResolver(_config_file=config_file)
	tst_str = "testStr"
	for day in default_config_dict["weekdays"]:
		resolver.edit_weekday_exts(day, lambda l: l.append(tst_str))
	
	for day in default_config_dict["weekdays"]:
		assert tst_str in resolver.day_exts(day)

	new_resolver = VideoNameResolver(_config_file=config_file)

	for day, day_exts in resolver.weekday_exts():
		assert day in default_config_dict["weekdays"]
		assert "testStr" in day_exts


def test_names_for_date(config_file):
	"""
	GIVEN
	WHEN
	THEN
	"""
	resolver = VideoNameResolver(_config_file=config_file)
	d = date(2021, 7, 5) #Monday July 5, 2021
	resolver.edit_general_exts(lambda exts: exts.append("NEW"))
	resolver.edit_weekday_exts("monday", lambda exts: exts.append("Mon"))

	date_names_gen = resolver.names_for_date(d)

	assert next(date_names_gen) == "210705"
	assert next(date_names_gen) == "210705_NEW"
	assert next(date_names_gen) == "210705_Mon"
	assert next(date_names_gen) == "210705_Mon_NEW"



def test_names_for_weekof(config_file, default_config_dict):
	"""
	GIVEN
	WHEN
	THEN
	"""
	

	resolver = VideoNameResolver(_config_file=config_file)
	d = date(2021, 7, 6) #Monday July 5, 2021

	resolver.edit_general_exts(lambda exts: exts.append("NEW"))
	for day in default_config_dict["weekdays"]:
		resolver.edit_weekday_exts(day, lambda exts: exts.append(day[:3].capitalize()))


	weekday_gen = resolver.names_for_weekof(d)

	expected_week_dates = [date(2021, 7, 5), date(2021, 7, 6), date(2021, 7, 7), date(2021, 7, 8), date(2021, 7, 9)]
	expected_weekday_names = [
		["210705","210705_NEW","210705_Mon","210705_Mon_NEW"],
		["210706","210706_NEW","210706_Tue","210706_Tue_NEW"],
		["210707","210707_NEW","210707_Wed","210707_Wed_NEW"],
		["210708","210708_NEW","210708_Thu","210708_Thu_NEW"],
		["210709","210709_NEW","210709_Fri","210709_Fri_NEW"]
	]

	for expected_date in expected_week_dates:
		d , names = next(weekday_gen)
		assert expected_date == d
		for expected_name in expected_weekday_names[expected_date.weekday()]:
			assert expected_name == next(names)
	



